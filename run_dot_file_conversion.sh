#!/bin/sh
# ------------------------------------------------------------
# This script converts dot files to png files,
# which were output by run_sdf_classification.
# ------------------------------------------------------------

dot -Tpng report/sdf_classification/decision_tree_3.dot -o report/sdf_classification/decision_tree_3.png
dot -Tpng report/sdf_classification/decision_tree_4.dot -o report/sdf_classification/decision_tree_4.png
dot -Tpng report/sdf_classification/decision_tree_5.dot -o report/sdf_classification/decision_tree_5.png
