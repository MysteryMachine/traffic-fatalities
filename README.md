# Traffic Fatalities

## Overview

According to the CDC, motor vehicle accidents are the #2 cause of accidental death in the U.S. In 2016, the National Highway Traffic Safety Administration recorded 37,461 traffic fatalities.

Understanding the conditions and behaviors that lead to motor vehicle accidents could lead to actions, legislation, and policy enforcement that could save lives.  Our team will research traffic related fatalities and traffic conditions.
Team
Our team, Mystery Machine, consists of 2 software engineers and data analysts.
* Bryan Castillo
* Rebecca Yang

This project is part of a class at the University Washington, CSS490 - Pattern Recognition.
This class running during Spring of 2018 at the U.W. Bothell campus.

## Goal
Our team’s goal is to identify factors which lead to increased traffic related fatalities. From this research we will propose actions to reduce fatalities. Some potential actions could include proposed speed limit changes, increased traffic enforcement, or dangerous condition notifications for drivers.

Our research will use a prediction model to run simulations where certain factors are changed such as speed or traffic volume. We will use these simulations to estimate new fatality counts. These simulations will be used to justify proposed actions.

A successful study will include:
1. Clearly communicated data and analysis showing the factors which influence traffic fatalities.
2. A reliable prediction model for fatalities.
3. A documented set of potential actions to reduce fatalities including simulations using the built models.

## Development Tools

The code to process data uses the following:

1. Python3 - The extraction scripts use python3 to process files.
2. Python Module: dbfread - Some data files use the DBF format and the Python module dbfread is required.
3. Octave - Octave is used for analysis.

### Python Modules

Some extra python modules are required. These can be installed using the pip command. pip install dbfread.

## Running Software

The software runs in multiple stages. Scripts are present in the top level
directory for running these stages. Shell scripts are present for Mac/\*nix,
while batch files are present for Windows.

The scripts primarily manage data stored in the data directory.

When running the software the scripts should be run at least once for each
previous stage.

### Stage 1: run_fars_decompress
```
Run: run_fars_decompress
```
This script will decompress FARS zip files, making the raw data available
for further processing.

### Stage 2: run_fars_load_db
```
Run: run_fars_load_db
```
This script will read the raw decompressed FARS data and will do the following:
1. It will determine the types for each column by inspecting the values.
2. It will determine the common tables and columns across all yearly FARS datasets.
3. It will create a SQL schema for the common tables and columns.
4. Using the generated schema, tables will be created in a sqlite database found under data/db/mmachine.db.
5. Finally, the script will load the FARS data into this database.

This process may take between 30 minutes to 1 and 1/2 hours. Stage 1 and Stage 2
can be skipped by decompressing data/db/mmachine.7z.

### Stage 3: run_create_views

```
Run: run_create_views
```

This script will create views of observations. These views include the
selected features that will be analyzed in later stages.

### Stage 4: run_create_text_data

```
Run: run_create_text_data single_driver_fatals
```

In the 4th stage text data files are created. These are text files extracted
form the sqlite datbas using queries. The data is written out to text files,
meant for use in octave scripts.

Note: This program requires an argument specifying which sql
file to dump to text data.

### Stage 5: initial_analysis_single_fatals.m

```
Run: octave-cli src\octave\initial_analysis_single_fatals.m
```
Runs initial basic analysis on single driver fatality observations.
