@echo off
REM ------------------------------------------------------------
REM This script converts dot files to png files,
REM which were output by run_sdf_classification.
REM ------------------------------------------------------------

set PATH=C:\Program Files (x86)\Graphviz2.38\bin;%PATH%
dot -Tpng report\sdf_classification\decision_tree_3.dot -o report\sdf_classification\decision_tree_3.png
dot -Tpng report\sdf_classification\decision_tree_4.dot -o report\sdf_classification\decision_tree_4.png
dot -Tpng report\sdf_classification\decision_tree_5.dot -o report\sdf_classification\decision_tree_5.png
