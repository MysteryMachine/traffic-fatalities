-- Categorizing accident types.
SELECT
  CASE
    WHEN PEDS = 0 AND PERSONS = 1 THEN '1 Vehicle: Single Occupant'
	  WHEN PEDS > 0 THEN 'Pedestrian'
	  WHEN PERSONS >= 8 AND PEDS = 0 THEN 'High Occupancy'
	  WHEN VE_FORMS = 1 AND PEDS = 0 AND PERSONS < 8 AND PERSONS > 1 THEN '1 Vehicle: Multiple Occupants'
	  WHEN VE_FORMS = 2 AND PEDS = 0 AND PERSONS < 8 THEN '2 Vehicles'
	  ELSE 'Other'
  END AS Accident_Type,
  COUNT(*) AS Count,
  100.0 * COUNT(*) / (SELECT COUNT(*) FROM FARS_ACCIDENT) AS Percent
FROM FARS_ACCIDENT
GROUP BY Accident_Type
ORDER BY COUNT(*) DESC
