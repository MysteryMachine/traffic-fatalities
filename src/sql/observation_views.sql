

-- Fatal accidents involving 1 person.
DROP VIEW IF EXISTS V_SINGLE_DRIVER_FATALS;
CREATE VIEW V_SINGLE_DRIVER_FATALS AS
SELECT
  a.DAY_WEEK,
  a.MONTH,
  a.HOUR,
  a.WEATHER,
  p.SEX,
  p.AGE,
  p.RACE,
  p.REST_USE,  -- Was restraint (seatbelt) used?
  p.REST_MIS,  -- Was restraint misused.
  p.DRINKING,  -- Police reported drinking
  p.DRUGS,     -- Police reported drugs
  v.BODY_TYP,  -- Vehicle body type
  v.TRAV_SP,   -- The travel speed.
  v.VSPD_LIM,  -- The speed limit
  v.PREV_ACC,  -- Previous recorded crashes
  v.PREV_SUS,  -- Previous suspensions
  v.PREV_DWI,  -- Previous DWI convictions
  v.PREV_SPD,  -- Previous speeding convictions
  p.AIR_BAG,   -- Air bag deployment
  CASE WHEN p.DOA > 0 THEN 1 ELSE 0 END as DOA
FROM FARS_ACCIDENT a
INNER JOIN FARS_PERSON p ON (
  a.ST_CASE = p.ST_CASE
)
INNER JOIN FARS_VEHICLE v ON (
  a.ST_CASE = v.ST_CASE
)
WHERE
  a.PERSONS = 1 AND
  a.PEDS = 0 AND
  p.DOA IN (0,7,8);
