%-------------------------------------------------------------------------------
% Dumps out a CSV for the single driver fatal feature name to feature index.
%-------------------------------------------------------------------------------
function mm_dump_sdf_feature_index_key()
    % Initialize the directory for output
    output_dir = "report";
    mkdir(output_dir);
  
    % Load data
    ds = mm_single_driver_fatals.load();
    
    file_name = sprintf("%s/feature_index_key.csv", output_dir);
    printf("Writing the feature index key to: %s\n", file_name);
    
    % Open the file.
    fh = fopen(file_name, "w");
    fprintf(fh, "Index,Name\n");
    for i = 1:length(ds.columns)
      fprintf(fh, "%d,%s\n", i, ds.columns{i});
    endfor
    fclose(fh);
endfunction
