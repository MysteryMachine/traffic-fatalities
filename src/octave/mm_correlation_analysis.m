%-------------------------------------------------------------------------------
% Writes out report data and graphs showing how features correlation with
% each other.
%-------------------------------------------------------------------------------
function mm_correlation_analysis()
  
    % Initialize the directory for output
    output_dir = "report/feature_correlation";
    mkdir(output_dir);
  
    % Load data
    ds = mm_single_driver_fatals.load();
    
    % Writes out the correlation matrix color maps.
    mm_generate_correlation_color_maps(output_dir, ds);
    
    % Output CSV data for correlation values.
    report_important_correlation_csv(
        ds,
        sprintf("%s/moderate_to_high_feature_correlation.csv", output_dir));
endfunction

% Writes out the correlation values to a csv.
function report_important_correlation_csv(ds, file_name)
    % Open the file for writing.
    printf("Writing correlation info to %s.\n", file_name);
    fh = fopen(file_name, "w");
  
    % Get the correlation matrices.
    cm_doa_0 = corr(ds.data(ds.get_col("DOA") == 0, :));
    cm_doa_1 = corr(ds.data(ds.get_col("DOA") == 1, :));
    
    % Show the higher correlation values
    cmi_doa_0 = get_correlation_indices(cm_doa_0);
    cmi_doa_1 = get_correlation_indices(cm_doa_1);
    
    fprintf(fh, "Corr. Indicator,Corr. Value,Feature 1,Feature 2,DOA\n");
    
    % moderate correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_0,
        0,
        "moderate",
        (@(v) abs(v) > 0.5 & abs(v) <= 0.8));
        
    % high correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_0,
        0,
        "high",
        (@(v) abs(v) > 0.8));
    
    % moderate correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_1,
        1,
        "moderate",
        (@(v) abs(v) > 0.5 & abs(v) <= 0.8));
        
    % high correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_1,
        1,
        "high",
        (@(v) abs(v) > 0.8));
        
    % low correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_0,
        0,
        "low",
        (@(v) abs(v) < 0.5));
        
    % low correlation
    report_correlation_stats(
        fh,
        ds,
        cmi_doa_1,
        1,
        "low",
        (@(v) abs(v) < 0.5));
        
    fclose(fh);
endfunction

% Write out the correaltion stats under a certain condition to fh.
function report_correlation_stats(fh, ds, cmi, doa, name, predicate)
    % moderate correlation
    indices = predicate(cmi(:, 3));
    if (sum(indices) >= 1)
        mod_cmis = cmi(indices, :);
        for i = 1:rows(mod_cmis)
            f1_i = mod_cmis(i, 1);
            f2_i = mod_cmis(i, 2);
            f1_n = ds.columns{f1_i};
            f2_n = ds.columns{f2_i};
            fprintf(fh, "%s,%0.3f,%s,%s,%d\n", name, mod_cmis(i, 3), f1_n, f2_n, doa);
        endfor
    endif
endfunction

% Generates a matrix color map for correaltion coefficients.
function mm_generate_correlation_color_maps(output_dir, ds)
    % Get the correlation matrices.
    cm_doa_0 = corr(ds.data(ds.get_col("DOA") == 0, :));
    cm_doa_1 = corr(ds.data(ds.get_col("DOA") == 1, :));

    % Correlation for doa = 1
    create_correlation_color_map(
        cm_doa_1,
        "Feature Correlation: Dead on Arrival",
        sprintf("%s/feature_correlation_matrix_heatmap_doa_1.png", output_dir));
    
    % Correlation for doa = 0
    create_correlation_color_map(
        cm_doa_0,
        "Feature Correlation: Died at Hospital",
        sprintf("%s/feature_correlation_matrix_heatmap_doa_0.png", output_dir));
endfunction

% Creates a single correlation coefficient color map.
function create_correlation_color_map(cm, graph_title, file_name)
  
    % The last column is DOA and is not going to be graphed.
    n = rows(cm) - 1;
  
    font_size = 20;
    tick_font_size = 14;
  
    clf('reset');
    
    % Set the tick font size
    set(gca, "fontsize", tick_font_size);
    
    title(graph_title, "fontsize", font_size);
    xlabel("Feature Index", "fontsize", font_size);
    ylabel("Feature Index", "fontsize", font_size);
    set (gca, "ydir", "reverse") 
    axis([0.5 (0.5 + n) 0.5 (0.5 + n)]);
    hold on;
    set(gca, 'xtick', 1:n);
    set(gca, 'ytick', 1:n);
  
    for row_n = 1:n
        for col_n = 1:n
            if row_n < col_n
                cv = cm(row_n, col_n);
                color = get_correlation_color(cv);
                vertices = [
                  (col_n - 0.5) (row_n - 0.5);
                  (col_n + 0.5) (row_n - 0.5);
                  (col_n + 0.5) (row_n + 0.5);
                  (col_n - 0.5) (row_n + 0.5)
               ];
               fill(vertices(:,2), vertices(:,1), color);
            endif
        endfor
    endfor

    
    printf("Writing graph to %s.\n", file_name);
    print(file_name);
end

% Assigns a color for a correlation value.
function color = get_correlation_color(cv)
    acv = abs(cv);
    if acv <= 0.5
        % low correlation is a gradient between white and green.
        color = [(1 - acv / 0.5) 1 (1 - acv / 0.5)];
    elseif acv > 0.5 & acv <= 0.8
        % moderate correlation is a graident from yellow to red.
        color = [1 (1 - (acv - 0.5) / 0.3) 0];
    else
        % Just black
        color = [0 0 0];
    endif
endfunction

% Returns a flattened array of indices and correlation values.
function results = get_correlation_indices(cvs)
    results = [];
    n = rows(cvs) - 1;
    for row_n = 1:n
        for col_n = 1:n
            if row_n > col_n
                cv = cvs(row_n, col_n);
                results = [results; row_n col_n cv];
            endif
        endfor
    endfor
endfunction


