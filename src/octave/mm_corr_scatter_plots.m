%-------------------------------------------------------------------------------
% Prints out scatter plots between 2 features.
% The plots only print out for features with a high enough correlation coefficient.
% Because there are many samples the graphs show points based on multiple points.
% All points at a given x and y value combination for the 2 features are grouped
% and a point size and point color is assigned:
%    * The point color ranges from green to red based on the DOA probability.
%      A DOA probability of 0 is assigned green and 1 is red. Andy value between
%      is a gradient value between green and red.
%    * The size of the point is based on the number of observations seen at that
%      point.
%-------------------------------------------------------------------------------
function mm_corr_scatter_plots()
  
    % Initialize the directory for output
    output_dir = "report/corr_scatter_plots"; 
    mkdir(output_dir);
  
    % Load data
    ds = mm_single_driver_fatals.load();
    
    doa_col = ds.get_col("DOA");
    
    % Get the correlation matrix.
    cm = corr(ds.data);
    
    n_features = length(ds.columns) - 1;
    font_size = 18;
    tick_font_size = 14;
    line_width = 1;
    
    total_n = rows(ds.data);
    
    marker_min_size = 20;
    marker_max_size = 4000;
    
    % Loop over feature combinations.
    for f1_i = 1:n_features
        for f2_i = (f1_i+1):n_features
          
            % Skip over feature combinations with a low correlation coeeficient.
            corr_v = cm(f1_i, f2_i);
            if abs(corr_v) <= 0.15
                continue
            endif
          
            col1_name = ds.columns{f1_i};
            col1_data = ds.data(:, f1_i);
            col1_disp = strrep(col1_name, "_", " ");
            
            col2_name = ds.columns{f2_i};
            col2_data = ds.data(:, f2_i);
            col2_disp = strrep(col2_name, "_", " ");
          
            % Get the unique values along both axes.
            c1_values = unique(col1_data);
            c2_values = unique(col2_data);
            
            % Get the axes range.
            c1_min = min(c1_values);
            c2_min = min(c2_values);
            c1_max = max(c1_values);
            c2_max = max(c2_values);

            clf('reset');
            grid on;
            hold on;
            axis([c1_min c1_max c2_min c2_max]);
            set(gca, "fontsize", tick_font_size);
            
            % Loop over unique feature value combinations.
            for c1_v = c1_values'
                for c2_v = c2_values'
                    
                    % How many observations have these values?
                    pair_n = sum(col1_data == c1_v & col2_data == c2_v);
                    if pair_n == 0
                        continue
                    endif
                    
                    % Compute the DOA probability.
                    pair_doa = sum(col1_data == c1_v & col2_data == c2_v & doa_col == 1);
                    pair_doa_p = pair_doa / pair_n;
                    
                    % Create the color based on probabability.
                    m_color = [pair_doa_p (1 - pair_doa_p) 0];
                    
                    % Create size of the point based on the number of observations.
                    m_size = (pair_n / total_n) * (marker_max_size - marker_min_size) + marker_min_size;
                    
                    % Draw the single point.
                    r = scatter([c1_v], [c2_v], m_size, m_color);
                    set(r, 'markerfacecolor', m_color);
                    
                endfor
            endfor
            
            % Overal all graph display.
            title(sprintf("%s to %s: Corr = %0.2f", col1_disp, col2_disp, corr_v), "fontsize", font_size);
            xlabel(col1_disp, "fontsize", font_size);
            ylabel(col2_disp, "fontsize", font_size);
            
            % Write the graph out.
            file_name = sprintf("%s/%02d_%02d.feature_value_p_doa_%s_%s.png",
                output_dir,
                f1_i,
                f2_i,
                col1_name,
                col2_name);
            printf("Writing graph to %s.\n", file_name);
            print(file_name);
        endfor
    endfor
endfunction
