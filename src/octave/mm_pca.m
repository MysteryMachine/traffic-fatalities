%------------------------------------------------------------------------------
% runs pca analysis.
%------------------------------------------------------------------------------

function mm_pca()
    printf("Starting PCA analysis.\n");
    % Initialize the directory for output
    output_dir = "report/pca_analysis";
    mkdir(output_dir);

    % Load data
    ds = mm_single_driver_fatals.load();

    % get the observation data not labelled.
    obs_data = ds.data(:, !ismember(ds.columns, "DOA"));
    
    % Mean-center/scale each feature, X is NORMALIZED & MEAN CENTERED dataset
    X = (mean(obs_data) - obs_data) ./ std(obs_data);

    % Singular value decomposition.
    [U S V] = svd(X, 0);

    % Ur is the transformed dataset.
    Ur = U * S;
    
    make_scree_plots(S, output_dir);
    make_loading_vector_plots(V, output_dir);

    tags = ds.data(:, columns(ds.data));

    output_dir2 = "report/pca_analysis/scatter_2d";
    mkdir(output_dir2);
    make_2d_scatter_plots(tags,Ur,output_dir2);
    
    output_dir3 = "report/pca_analysis/scatter_3d";
    mkdir(output_dir3);
    make_3d_scatter_plots(tags,Ur,output_dir3);
    
    % Show which features are needed.
    disp(determine_needed_features(V, 14));
    
endfunction

% Generates 2-D scatter plots.
function make_2d_scatter_plots(tags, Ur, plot_dir)
    for i = 1:columns(Ur)
        for j = (i+1):columns(Ur);
            clf('reset');
            set(gca, "fontsize", 20);
            title(sprintf("Principal Component: %d to %d", i, j));
            hold on;
            xlabel(sprintf("PC %d", i));
            ylabel(sprintf("PC %d", j));
            
            scatter(Ur(tags == 0,i), Ur(tags == 0,j), 'r');
            scatter(Ur(tags == 1,i), Ur(tags == 1,j), 'b');

            legend({"Non-Fatal", "Fatal"});
            
            % Save the graph
            file_name = sprintf("%s/scatter_2d_%d_%d.png", plot_dir, i, j);
            printf("Writing plot to: %s\n", file_name);
            print(file_name);
        endfor
    endfor
endfunction

% Generates 3-D scatter plots.
function make_3d_scatter_plots(tags, Ur, plot_dir)
    nfeatures = columns(Ur);
    for x = 1:nfeatures
        for y = x+1:nfeatures
            for z = y+1:nfeatures
                make_3d_scatter_plot(tags, Ur, [x y z], plot_dir);
            endfor
        endfor
    endfor
endfunction

% Generate 1 of the 3-D scatter plots.
function make_3d_scatter_plot(tags, Ur, pc, plot_dir)
    clf('reset');
    scatter3(Ur(tags == 0, pc(1)), Ur(tags == 0, pc(2)), Ur(tags == 0, pc(3)), 'r');
    hold on;
    scatter3(Ur(tags == 1, pc(1)), Ur(tags == 1, pc(2)), Ur(tags == 1, pc(3)), 'b');

    set(gca, "fontsize", 20);
    title(sprintf("Principal Components: %d,%d,%d", pc(1), pc(2), pc(3)));
    hold on;
    xlabel(sprintf("PC %d", pc(1)));
    ylabel(sprintf("PC %d", pc(2)));
    zlabel(sprintf("PC %d", pc(3)));
    
    file_name = sprintf("%s/scatter_3d_%d_%d_%d.png", plot_dir, pc(1), pc(2), pc(3));
    printf("Writing plot to: %s\n", file_name);
    print(file_name);
endfunction


% Examines loading vector values to determine which feature indices
% are needed. This is done by looking at the contribution of each
% feature for n number of principal components. If the absolute value
% in the loading vector is close to the same then the first feature index
% is counted as needed. The exception is that features contributing close to
% zero are not counted either.
%
% This is run up to N principal components. The final result returned indicates
% 1 for features that are needed, and 0 for those not needed.
function needed_features = determine_needed_features(V, n)
    abs_v = abs(V);
    max_v = max(max(abs_v));
    step = max_v / 25;
    
    % Initialize to none wanted.
    needed_features = zeros(1, rows(V));
    
    for i = 1:n
        v = abs_v(:,i);
        % Bucketize the values into specific increments.
        bucket_v = round(v / step) * step;
        % Loop through the unique increment values.
        unique_bv = unique(bucket_v);
        for j = 1:length(unique_bv)
            if unique_bv(j) > 0
                % if the increment value is not close to zero, find the first
                % occurrence of that value and count that index as a needed
                % feature.
                needed_feature = find(bucket_v == unique_bv(j))(1);
                needed_features(needed_feature) = 1;
            endif
        endfor
    endfor
endfunction

function make_scree_plots(S, output_dir)
    % Compute the scree and scree cumulative values.
    n_features = columns(S);
    S2 = S^2;
    weights = sum(S2) / sum(sum(S2));
    c_weights = cumsum(weights);

    # Generate the graph.
    clf('reset');
    set(gca, "fontsize", 14);
    title('Scree Plots');
    hold on;
    grid on;
    xlabel('Principal Component #');
    ylabel('Ratio of Variance');
    plot(weights, 'x:b');
    plot(c_weights, 'x:r');
    legend({"Scree Ratio", "Scree Ratio Cumulative"}, "location", "east");
    legend boxoff;
    set(gca, 'xtick', 1:n_features);
    
    % Save the graph
    file_name = sprintf("%s/scree_plot.png", output_dir);
    printf("Writing plot to: %s\n", file_name);
    print(file_name);
endfunction

% Generates the loading vector plots.
% The plots will be written out as image files.
function make_loading_vector_plots(V, output_dir)
  
    % Create a square with the sign retained.
    Vsquare = (V .^ 2) .* sign(V);

    % The following code plots a bar chart of the first loading vector
    % (it is a column vector because we didn't transpose V or Vsquare
    % (if you decide to transpose, then you will use Vsquare( 1, : ) instead).
    nfeatures = columns(V);
    for i = 1:nfeatures
        clf('reset');
        bar(Vsquare(i,:), 0.5);
        set(gca, "fontsize", 16);
        hold on;
        grid on;
        ymin = min(Vsquare(:,i)) + (min(Vsquare(:,i))/10);
        ymax = max(Vsquare(:,i)) + (max(Vsquare(:,i))/10);
        %axis([0 nfeatures ymin ymax]);
        xlabel('Feature Index');
        ylabel('Importance of Feature');
        title(sprintf('Loading Vector %d',i));
        
        % Save the graph
        file_name = sprintf("%s/loading_vector_plot_%d.png", output_dir, i);
        printf("Writing plot to: %s\n", file_name);
        print(file_name);
    endfor 

endfunction
