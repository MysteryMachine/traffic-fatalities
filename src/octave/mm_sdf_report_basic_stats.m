%------------------------------------------------------------------------------
% Loads and outputs some basic analysis of single driver fatals.
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% Run program.
%------------------------------------------------------------------------------
function mm_sdf_report_basic_stats()
    ds = mm_single_driver_fatals.load();
    printf("Columns:\n");
    ds.columns

    printf("Data contains %d rows and %d columns.\n",
        rows(ds.data), columns(ds.data));
        
    write_value_stats(ds, "data/single_driver_fatals_value_stats.csv");
    write_value_frequency(ds, "data/single_driver_fatals_value_frequencies.csv");
endfunction

%------------------------------------------------------------------------------
% Writes out frequency counts for each value in a column.
%------------------------------------------------------------------------------
function write_value_stats(ds, fname)
    printf("Writing to %s\n", fname);
    fh = fopen(fname, "w");
    fprintf(fh, "COLUMN,DISTINCT_VALUES,MIN,MAX,MEDIAN,MEAN,STDDEV\n");
    for i = 1:length(ds.columns)
        cname = ds.columns{i};
        cvalues = ds.data(:, i);
        
        % Remove unknowns
        cvalues = cvalues(cvalues != -1);
        
        ucvalues = unique(cvalues);
        min_v = min(ucvalues);
        max_v = max(ucvalues);
        mean_v = mean(cvalues);
        std_v = std(cvalues);
        fprintf(fh, "%s,%d,%s,%s,%s,%s,%s\n",
          cname,
          length(ucvalues),
          num2str(min(ucvalues)),
          num2str(max(ucvalues)),
          num2str(median(cvalues)),
          num2str(mean(cvalues)),
          num2str(std(ucvalues)));
    endfor
    fclose(fh);
endfunction

%------------------------------------------------------------------------------
% Writes out stats on value frequency.
%------------------------------------------------------------------------------
function write_value_frequency(ds, fname)
    printf("Writing to %s\n", fname);
    fh = fopen(fname, "w");
    fprintf(fh, "FEATURE,VALUE,COUNT,PERCENT,PERCENT_DOA\n");
    doa_v = ds.get_col("DOA") == 1;
    for i = 1:length(ds.columns)
        cname = ds.columns{i};
        cvalues = ds.data(:, i);
        ucvalues = unique(cvalues);

        % get the frequency count
        ucvalues(:,2) = arrayfun(@(v) sum(cvalues == v), ucvalues(:,1));

        % get percentage
        ucvalues(:,3) = 100 .* ucvalues(:,2) ./ rows(cvalues);

        % get percent that were doa
        doa_counts = arrayfun(@(v) sum(cvalues == v & doa_v), ucvalues(:,1));

        ucvalues(:,4) = 100 .* doa_counts ./ ucvalues(:,2);

        for x = 1:rows(ucvalues)
            fprintf(fh,"%s,%s,%s,%s,%s\n",
                cname,
                num2str(ucvalues(x,1)),
                num2str(ucvalues(x,2)),
                num2str(ucvalues(x,3)),
                num2str(ucvalues(x,4)));
        endfor

    endfor
    fclose(fh);
endfunction
