%------------------------------------------------------------------------------
% A class definition for data including column names.
% This makes it easier to process and handle data as columns are added
% or changed over time.
%------------------------------------------------------------------------------
classdef mm_dataset

  properties
    % The raw data
    data;
    
    % The columns for the matrix.
    columns;
  endproperties
  
  methods
  
    % Constructor
    function self = mm_dataset(data, columns)
      self.data = data;
      self.columns = columns;
    endfunction
    
    % Get a column by name.
    function column = get_col(self, name)
      column = self.data(:, self.get_col_index(name));
    endfunction
    
    % Get a column index by name.
    function i = get_col_index(self, name)
      i = find(ismember(self.columns, name));
    endfunction
    
  endmethods
  
  methods (Static = true)
  
    % Loads a dataset for a given name from the data directory.
    % The data is loaded from data/<data_name>.txt and the columns are from
    % data/<data_name>_meta.txt
    function self = load(data_name)
      data = load("-ascii", sprintf("data/%s.txt", data_name));
      columns = textread(sprintf("data/%s_meta.txt", data_name), "%s");
      self = mm_dataset(data, columns);
    endfunction
    
  endmethods
  
endclassdef
