%-------------------------------------------------------------------------------
% Prints out graphs showing feature value doa probability.
%-------------------------------------------------------------------------------
function mm_feature_value_doa_probability_graphs()
  
    % Initialize the directory for output
    output_dir = "report/feature_value_doa_probability";
    mkdir(output_dir);
  
    % Load data
    ds = mm_single_driver_fatals.load();
    doa_col = ds.get_col("DOA");
  
    % Get the overall p(DOA)
    p_doa = sum(doa_col == 1) / length(doa_col);
    printf("p(doa) = %0.3f\n", p_doa);
    
    n_features = length(ds.columns) - 1;
    font_size = 24;
    tick_font_size = 18;
    line_width = 4;
    
    % Loop through each feature.
    for i = 1:n_features
        col_name = ds.columns{i};
        col_data = ds.data(:, i);
        col_disp = strrep(col_name, "_", " ");
        
        value_stats = get_value_stats(col_data, doa_col);
        
        clf('reset');
        r = plot(value_stats(:,1), value_stats(:,2) ./ value_stats(:,3));
        set(r, "linewidth", line_width);
        grid on;
        hold on;
        title(sprintf("Probability of Death on Arrival"), "fontsize", font_size);
        xlabel(col_disp, "fontsize", font_size);
        ylabel(sprintf("p(DOA | %s)", col_disp), "fontsize", font_size);
        
        % Add the base doa probability line.
        r = line([min(col_data), max(col_data)], [p_doa p_doa]);
        set(r, "linewidth", line_width);
        set(r, "color", "r");
        
        % Make tick marks bigger.
        set(gca, "fontsize", tick_font_size);
        
        file_name = sprintf("%s/%02d.feature_value_p_doa_%s.png", output_dir, i, col_name);
        printf("Writing graph to %s.\n", file_name);
        print(file_name);
    endfor
endfunction

% Get probability stats for unique values.
function result = get_value_stats(values, doa_col)
    result = [];
    for v = unique(values)'
        num_v = sum(values == v);
        num_doa = sum(values == v & doa_col);
        result = [result; v num_doa num_v];
    endfor
endfunction