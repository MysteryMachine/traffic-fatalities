%------------------------------------------------------------------------------
% Static methods for clean and handling single driver fatal data.
%------------------------------------------------------------------------------
classdef mm_single_driver_fatals
  
  methods (Static = true)
  
    % Load the single driver fatals data.
    function ds = load()
      ds = mm_dataset.load("single_driver_fatals");
      ds = mm_single_driver_fatals.set_unknowns(ds);
    end
  
    % Sets unknown values to -1
    function new_ds = set_unknowns(ds)
      % unkown value definitions
      % This defines what values are considered unknown for a given field.
      uvd = struct();
      uvd.HOUR = [99];
      uvd.WEATHER = [98 99];
      uvd.SEX = [8 9];
      uvd.AGE = [998 999];
      uvd.RACE = [97 98 99];
      uvd.REST_USE = [97 98 99];
      uvd.DRINKING = [8 9];
      uvd.DRUGS = [8 9];
      uvd.BODY_TYP = [12 13 23 42 65 73 90 91 92 93 94 95 97 98 99];
      uvd.TRAV_SP = [997 998 999];
      uvd.VSPD_LIM = [98 99];
      uvd.PREV_ACC = [98 99 998 10000000000000000];
      uvd.PREV_SUS = [99 998 10000000000000000];
      uvd.PREV_DWI = [99 998 10000000000000000];
      uvd.PREV_SPD = [99 998 10000000000000000];
      uvd.AIR_BAG = [98 99];
  
      data = ds.data;
  
      % Loop through the unknown value definintions and apply them to the data.

      for [unknown_values, field_name] = uvd
        i = ds.get_col_index(field_name);
    
        % Which rows have an unknown value for this feature?
        unknown_rows = ismember(ds.data(:, i), unknown_values);
    
        data(unknown_rows, i) = -1;
      endfor
    
      new_ds = mm_dataset(data, ds.columns);
    endfunction
    
  endmethods
  
endclassdef