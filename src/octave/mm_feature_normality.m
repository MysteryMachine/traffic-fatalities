% octave-cli -p src/octave
%------------------------------------------------------------------------------
% Tell octave this is a script file: not a function file.
1;

%------------------------------------------------------------------------------
% Creates a normal distribution plot with each feature and species
% overlaid with a bar chart. The bar chart is the histogram where the
% area of the bar chart is normalized to 1.
%------------------------------------------------------------------------------
function create_bar_with_normal_dist_plot(data)
    min_d = min(data);
    max_d = max(data);
    step = (max_d - min_d) / 5;
    
    edges = min_d:step:max_d;
    counts = histc(data, edges);
    
    % Dividing by the sum and the width of the bar
    % makes sure that the area within the bars is 1.
    % this means it should match the area of a pdf and it makes
    % it easier to see if the actual data matches with a normal
    % distribution.
    bar(edges, counts ./ sum(counts) ./ step);
    hold on;

    mu = mean(data);
    st = std(data);
    x = linspace(mu - st*3, mu + st*3, 100);
    plot(x, normpdf(x, mu, st));
    set(gca,'fontsize',16);
endfunction

%------------------------------------------------------------------------------
% This method plots one feature per figure, saves each plot, then closes 
% each figure window. DOA and !DOA are generated in separate figures.
%------------------------------------------------------------------------------
function plot_feature(ds,ptype,fmax,nbins)
    [s1 f1] = size(ds.data);

    % Ensuring valid iteration
    if (fmax<1 || fmax>=f1)
        fmax = f1-1;
    endif

    for i = 1:(fmax*3)
        % Determining feature index
        f_i = idivide(i-1,3,'fix')+1;

        % obtaining DOA/!DOA data
        if (mod(i,3)==2) % DOA
            target_m = ds.data(ds.get_col("DOA")==1,:);
            dtype = 'DOA';
        else % !DOA
            target_m = ds.data(ds.get_col("DOA")==0,:);
            dtype = '!DOA';
        endif
        target = target_m(:,f_i);

        % normalized plot vs historgram plot
        if (ptype=='norm')
            create_bar_with_normal_dist_plot(target)
        else
            if(nbins==0)
                hist(target);
            else
                hist(target,nbins);
            endif
        endif

        t = title(strcat(ds.columns(f_i),' (',dtype,')'),'fontsize',20);
        % remove default interpreter that reads underlines as subscripts
        set(t,'Interpreter','none');

        xlabel('SAS Value','fontsize',16);
        ylabel('Normalized Frequency','fontsize',16);
        

        save_plot(strcat(dtype,num2str(f_i),ptype));
        close;
    endfor
endfunction

%------------------------------------------------------------------------------
% Print ("save") plot in given directory
% Example: filename: feature_normality_1to5.png
%------------------------------------------------------------------------------
function save_plot(filename1)
    output_dir = "report/feature_normality";
    mkdir(output_dir);
    filename = sprintf("%s/plot_%s.png",output_dir,filename1);
    print(filename);
endfunction

%------------------------------------------------------------------------------
% Main method
%------------------------------------------------------------------------------
function main(ptype,fmax,nbins)
    ds = mm_single_driver_fatals.load();
    plot_feature(ds,ptype,fmax,nbins);
endfunction

main('norm',0,0);






