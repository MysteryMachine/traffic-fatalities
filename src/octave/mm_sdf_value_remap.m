%------------------------------------------------------------------------------
% Remaps single driver fatals to alternate values based on probability.
% This class is used to experiment with reassigning categorical values.
%
% After trying this code out, I didn't find that remapping the values gave much
% better results. It gave slightly better capture of variation for a few
% principal components and a little more correlation for a few features, but
% it didn't really help that much.
%
%------------------------------------------------------------------------------
classdef mm_sdf_value_remap
  
  properties
    mapping;
  endproperties
  
  methods
    
    % Constructor
    function self = mm_sdf_value_remap(ds)
      self.mapping = mm_sdf_value_remap.get_mapping(ds);
    endfunction
  
    function new_ds = map_dataset(self, ds)
      data = ds.data;
      for i = 1:(columns(ds.data) - 1)
        data(:, i) = self.map_values(i, data(:, i));
      endfor
      new_ds = mm_dataset(data, ds.columns);
    endfunction
  
    function remapped = map_values(self, idx, values)
      m = self.mapping{idx};
      remapped = sum((values == m) .* (1:length(m)), 2);
    endfunction
  
    function unmapped = unmap_values(self, idx, values)
      m = self.mapping{idx};
      unmapped = m(values)';
    endfunction
  
  endmethods
  
  methods (Static = true)
    
    function result = get_mapping(ds)
      % Read the probabbility stats for value and get a vector for
      % each sset of unique values based on increasing probability.
      result = {};
      stats = mm_sdf_value_remap.get_column_value_stats(ds);
      for i = 1:length(stats)
        result{i} = stats{i}(:,1)';
      endfor
    endfunction
    
    function stats = get_column_value_stats(ds)
      % Get the dead on arrival column.
      doa_col = ds.get_col("DOA");
  
      % Get the overall p(DOA)
      p_doa = sum(doa_col == 1) / length(doa_col);
      
      % Loop through each feature.
      n_features = length(ds.columns) - 1;
      stats = {};
      for i = 1:n_features
        col_data = ds.data(:, i);
        stats{i} = mm_sdf_value_remap.get_value_stats(col_data, doa_col);
      endfor
      
    endfunction
    
    % Get probability stats for unique values.
    function result = get_value_stats(values, doa_col)
      result = [];
      for v = unique(values)'
        num_v = sum(values == v);
        num_doa = sum(values == v & doa_col);
        result = [result; v num_doa / num_v];
      endfor
      result = sortrows(result, 2);
    endfunction
    
  endmethods
  
endclassdef
