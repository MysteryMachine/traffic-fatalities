from mmachine.file_tables import *
from mmachine.fars.file_sets import *
from mmachine.fars.schema_info import *
from mmachine.sqldb import *
import os
import io

# Creates the schema and loads data into a sqlite db.
class SqlLoader:

    def __init__(self, data_dir, detection_rows = -1):
        self.data_dir = data_dir
        self.detection_rows = detection_rows

    def create_schema(self):
        with SqlDb(self.data_dir).get_connection() as conn:
            c = conn.cursor()
            sql_gen = SqlSchemaGenerator(self.data_dir, self.detection_rows)
            for sql in sql_gen.get_sql_schema():
                c.execute(sql)

    def load_data(self):
        with SqlDb(self.data_dir).get_connection() as conn:
            sql_schema = self._get_sql_schema(conn)
            rfs = RawFiles(self.data_dir)
            for year in rfs.get_years():
                self._load_year(year, conn, rfs, sql_schema)

    def _load_year(self, year, conn, rfs, sql_schema):
        print("Loading FARS data for {}.".format(year))
        for table_name in sorted(sql_schema.keys()):
            self._load_table(table_name, year, conn, rfs, sql_schema)

    def _load_table(self, table_name, year, conn, rfs, sql_schema):
        raw_file = rfs.get_table_file(year, sql_schema[table_name]['raw_table_name'])
        print("Loading: {}".format(raw_file))
        tr = TableReader.open_with_type_detection(raw_file, self.detection_rows)
        commit_size = 5000
        insert_count = 0
        sql_insert = sql_schema[table_name]["sql_insert"]
        columns = sql_schema[table_name]["columns"]
        cursor = conn.cursor()
        with tr.rows() as rows:
            for row in rows:
                params = []
                for i in range(0, len(columns)):
                    col_name = columns[i]
                    if col_name == 'ST_CASE':
                        # Add year to make it unique
                        params.append(str(year) + '_' + str(row[col_name]))
                    else:
                        params.append(row[col_name])
                cursor.execute(sql_insert, params)
                insert_count += 1
                if (insert_count % commit_size) == 0:
                    conn.commit()
                    print("Committed {} rows to {} for year {}.".format(insert_count, table_name, year))
        if (insert_count % commit_size) != 0:
            conn.commit()
            print("Committed {} rows to {} for year {}.".format(insert_count, table_name, year))

    def _get_sql_schema(self, conn):
        table_info = {}
        for table_name in self._get_fars_tables(conn):
            table_info[table_name] = { 'columns': [] }
            for col in self._get_columns(conn, table_name):
                table_info[table_name]['columns'].append(col)
        self._create_insert_statements(table_info)
        self._add_raw_file_table_names(table_info)
        return table_info

    def _add_raw_file_table_names(self, table_info):
        for table_name in table_info.keys():
            # SQL tables start with FARS_ on the raw file table name.
            table_info[table_name]['raw_table_name'] = table_name[5:]

    def _create_insert_statements(self, table_info):
        for table_name in table_info.keys():
            sql = "INSERT INTO `{}` ".format(table_name)
            sql += "("
            values = "("
            columns = table_info[table_name]['columns']
            for i in range(0, len(columns)):
                if i > 0:
                    sql += ", "
                    values += ","
                sql += "`{}`".format(columns[i])
                values += "?"
            sql += ") VALUES " + values + ")"
            table_info[table_name]['sql_insert'] = sql

    def _get_columns(self, conn, table_name):
        sql = "PRAGMA table_info('{}')".format(table_name)
        c = conn.cursor()
        columns = []
        for row in c.execute(sql):
            columns.append(row[1])
        c.close()
        return columns

    def _get_fars_tables(self, conn):
        sql = """
           SELECT name
           FROM sqlite_master
           WHERE type = 'table' AND name LIKE 'FARS_%'
        """
        c = conn.cursor()
        tables = []
        for row in c.execute(sql):
            tables.append(row[0])
        c.close()
        return tables

# Creates a SQL schema for storing FARS data.
class SqlSchemaGenerator:

    def __init__(self, data_dir, detection_rows = -1):
        self.data_dir = data_dir
        self._schema_info = None
        self.detection_rows = detection_rows

    def get_schema_info(self):
        if self._schema_info == None:
            self.detect()
        return self._schema_info

    def detect(self):
        # Load only the supported tables and columns
        si = SchemaInfo(self.data_dir)
        self._schema_info = si.get_all_supported_with_type_detection(self.detection_rows)

    def get_sql_schema(self):
        schema_info = self.get_schema_info()
        sql_statements = []
        for table_name in sorted(schema_info.keys()):
            self._get_sql_schema_for_table(table_name, schema_info[table_name], sql_statements)
        return sql_statements

    def _get_sql_schema_for_table(self, table_name, column_info, sql_statements):
        # Tables are prefixed with FARS so that other tables from other
        # data sources can exist in the same database/schema.
        sql_statements.append("DROP TABLE IF EXISTS `FARS_{}`".format(table_name))
        sql = io.StringIO()
        sql.write('CREATE TABLE `FARS_{}` '.format(table_name))
        # This is on a separate line because it is messing with my editors highlighting.
        sql.write('(\n')
        for i in range(0, len(column_info)):
            ci = column_info[i]
            if ci['name'] == 'ST_CASE':
                sql.write("  `ST_CASE` VARCHAR(40) NOT NULL")
            else:
                sql.write("  `{}` ".format(ci['name']))
                if ci['type'] == float:
                    sql.write('DECIMAL')
                elif ci['type'] == int:
                    sql.write('INTEGER')
                elif ci['type'] == str:
                    sql.write('VARCHAR(255)')
                else:
                    raise Exception("Unknown type for column {} - {}.".format(ci['name'], ci['type']))
            if i < (len(column_info) - 1):
                sql.write(',\n')
        sql.write('\n)')
        sql_statements.append(sql.getvalue())

        sql = io.StringIO()
        sql.write('CREATE')
        if table_name == 'ACCIDENT':
            sql.write(' UNIQUE')
        sql.write(' INDEX `i_FARS_{}_ST_CASE` ON `FARS_{}` (ST_CASE)'.format(table_name, table_name))
        sql_statements.append(sql.getvalue())
