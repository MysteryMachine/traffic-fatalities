from mmachine.fars.file_sets import *
from mmachine.file_tables import *
import csv
import os

class SchemaInfo:

    def __init__(self, data_dir):
        self.data_dir = data_dir

    def get_all_supported_with_type_detection(self, max_rows = -1):
        # Load only the supported tables and columns
        schema_info = self.get_all_supported(self.get())
        tables = {}
        # Determine types for fields.
        rfs = RawFiles(self.data_dir)
        year = max(rfs.get_years())
        for tname in rfs.get_table_names(year):
            if tname in schema_info:
                # supported table
                tf = rfs.get_table_file(year, tname)
                tr = TableReader.open_with_type_detection(tf, max_rows)
                header = tr.header
                col_types = tr.converter.type_info.get_types()
                tables[tname] = []
                for col in tr.header:
                    if col in schema_info[tname]:
                        tables[tname].append({ 'name': col, 'type': col_types[col] })
        return tables

    def get(self):
        rfs = RawFiles(self.data_dir)
        schema_info = {}
        for year in rfs.get_years():
            for tname in rfs.get_table_names(year):
                tf = rfs.get_table_file(year, tname)
                with TableReader.open(tf) as tr:
                    self._update_schema_info(schema_info, year, tname, tr.header)
        return schema_info

    # Given a returned schema _type_info
    # Return the tables and columns supported for all years.
    def get_all_supported(self, schema_info):
        all_schema_info = {}
        all_years = RawFiles(self.data_dir).get_years()
        for table, columns in schema_info.items():
            for column, years in columns.items():
                if sum(years.values()) == len(all_years):
                    if table not in all_schema_info:
                        all_schema_info[table] = {}
                    all_schema_info[table][column] = 1
        return all_schema_info

    def write_csv(self, schema_info, csv_file):
        rfs = RawFiles(self.data_dir)
        years = sorted(rfs.get_years())
        years.reverse()

        with open(csv_file, "w", newline='') as fh:
            csv_w = csv.writer(fh)
            header = ['TABLE', 'FIELD']
            header.extend(years)
            header.append('ALL')
            csv_w.writerow(header)
            for tname in sorted(schema_info.keys()):
                for hname in sorted(schema_info[tname]):
                    row = [tname, hname]
                    all_supported = 1
                    for year in years:
                        if year in schema_info[tname][hname]:
                            row.append(1)
                        else:
                            row.append(0)
                            all_supported = 0
                    row.append(all_supported)
                    csv_w.writerow(row)

    def _update_schema_info(self, schema_info, year, tname, headers):
        if not tname in schema_info:
            schema_info[tname] = {}
        for header in headers:
            hname = header.upper()
            if not hname in schema_info[tname]:
                schema_info[tname][hname] = {}
            schema_info[tname][hname][year] = 1
