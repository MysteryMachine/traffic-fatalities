import re
import os
import os.path
import shutil
import zipfile

# Contains helpers for accessing and manipulating sets of file data.

# Access and manipulate the raw data files from FARS.
class RawFiles:

    def __init__(self, data_dir = "data"):
        self.data_dir = data_dir
        self._year_files = {}

    def get_dir(self):
        return os.path.join(self.data_dir, "FARS", "raw")

    def get_table_file(self, year, name):
        f = self._get_files(year)[name]
        return os.path.join(self.get_dir(), str(year), f)

    def get_table_names(self, year):
        return self._get_files(year).keys()

    def get_years(self):
        years = []
        raw_dir = self.get_dir()
        for f in os.listdir(raw_dir):
            if os.path.isdir(os.path.join(raw_dir, f)) and re.match(r'^\d{4}$', f):
                years.append(int(f))
        return years

    def extract(self, year, zip_filename):
        extract_dir = os.path.join(self.get_dir(), str(year))
        tmp_dir = extract_dir + "_tmp"

        if os.path.isdir(extract_dir):
            # Already extracted
            return

        if os.path.isdir(tmp_dir):
            shutil.rmtree(tmp_dir)
            os.makedirs(tmp_dir)

        with zipfile.ZipFile(zip_filename, 'r') as zf:
            print("Extracting to {}.".format(tmp_dir))
            zf.extractall(tmp_dir)

        print("Renamed extract dir to {}.".format(extract_dir))
        os.rename(tmp_dir, extract_dir)

    def _get_files(self, year):
        if year in self._year_files:
            return self._year_files[year]

        f_re = re.compile(r'(.*)\.(dbf|csv)$', re.IGNORECASE)
        raw_dir = self.get_dir()
        year_dir = os.path.join(raw_dir, str(year))
        year_files = {}
        for f in os.listdir(year_dir):
            m = f_re.match(f)
            if m:
                year_files[m.group(1).upper()] = f
        self._year_files[year] = year_files
        return year_files

# Access the downloaded zip files from FARs.
class DownloadFiles:

    def __init__(self, data_dir = "data"):
        self.data_dir = data_dir

    def get_dir(self):
        return os.path.join(self.data_dir, "FARS", "download")

    def get_files(self):
        download_dir = self.get_dir()
        download_file_re = re.compile(r"^FARS(\d{4}).*\.zip$", re.IGNORECASE)
        for f in os.listdir(download_dir):
            m = download_file_re.match(f)
            if m:
                yield { "file": os.path.join(download_dir, f), "year": int(m.group(1)) }
