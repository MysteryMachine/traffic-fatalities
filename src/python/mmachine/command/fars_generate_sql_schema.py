from mmachine.fars.sql import *

class FarsGenerateSqlSchema:

    def run(self):
        sql_gen = SqlSchemaGenerator("data")
        sql_statements = sql_gen.get_sql_schema()
        for sql in sql_statements:
            print(sql + ";")

if __name__ == "__main__":
    FarsGenerateSqlSchema().run()
