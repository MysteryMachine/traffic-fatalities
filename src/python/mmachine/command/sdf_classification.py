#===============================================================================
# Script: sdf_classification.py
# Authors: Bryan Castillo and Rebecca Yang
# Description:
#
#
#===============================================================================
import os
import io
import datetime

# Exta python libraries often used for machine learning.
import numpy
import pandas
import sklearn.tree
import sklearn.neighbors
import sklearn.neural_network
import sklearn.ensemble
import sklearn.svm
import sklearn.decomposition
import sklearn.discriminant_analysis

class SDFClassification:
    
    # Constructor
    def __init__(self, data_dir, output_dir):
        self.data_dir = data_dir
        
        # Make sure an output directory exists.
        self.output_dir = output_dir
        if not os.path.isdir(self.output_dir):
            os.makedirs(self.output_dir)
        
        # Holds the information about test classification runs.
        self.runs = []

    #================================================================
    # Reading source data
    #================================================================

    # Reads a matrix of data from a database query.
    # The data and column names are returned.
    def read_data_sql(self, sql):
        # make the import lazy
        import sqlite3
        db_dir = os.path.join(self.data_dir, "db")
        db_file = os.path.join(db_dir, "mmachine.db")
        with sqlite3.connect(db_file) as conn:
            cur = conn.cursor()
            cur.execute(sql)
            columns = [col[0] for col in cur.description]
            rows = []
            for row in cur:
                rows.append([c for c in row])
            return (numpy.array(rows), columns)
    
    #================================================================
    # Data Pre-processing
    #================================================================
    
    # Normalize the values by centering them around the mean and dividing by
    # the standard deviation.
    def normalize_values(self, data):
        n_cols = len(data[0]) - 1
        xform_data = data[:, 0:n_cols]
        data_mean = numpy.mean(xform_data, axis=0)
        data_std = numpy.std(xform_data, axis=0)
        xform_data = (data_mean - xform_data) / data_std
        return numpy.array([numpy.append(xform_data[i], data[i, n_cols]) for i in range(0, len(xform_data))])

    # This method resets unknown values to -1.
    # The set of unknown values is based on the definition of the data fields
    # in the FARS dataset.
    def reset_unknown_values(self, data, column_names):
        # These values indicate that a value was not known or is ambiguous.
        unknown_values = {
            "HOUR": {99},
            "WEATHER": {98, 99},
            "SEX": {8, 9},
            "AGE": {998, 999},
            "RACE": {97, 98, 99},
            "REST_USE": {97, 98, 99},
            "DRINKING": {8, 9},
            "DRUGS": {8, 9},
            "BODY_TYP": {12, 13, 23, 42, 65, 73, 90, 91, 92, 93, 94, 95, 97, 98, 99},
            "TRAV_SP": {997, 998, 999},
            "VSPD_LIM": {98, 99},
            "PREV_ACC": {98, 99, 998, 10000000000000000},
            "PREV_SUS": {99, 998, 10000000000000000},
            "PREV_DWI": {99, 998, 10000000000000000},
            "PREV_SPD": {99, 998, 10000000000000000},
            "AIR_BAG": {98, 99}
        }

        # Run through the unknown value transformation
        n_rows = []
        for row in data:
            n_row = []
            for i in range(0, len(row)):
                col_name = column_names[i]
                v = row[i]
                if col_name in unknown_values and v in unknown_values[col_name]:
                    v = -1
                n_row.append(v)
            n_rows.append(n_row)

        # Convert to a numpy matrix.
        return numpy.array(n_rows)

    # Split the data randomly and return 75% of the rows as the training data
    # and 25% as test data.
    def split_data(self, data):
        data = data.copy()
        numpy.random.shuffle(data)
        index_cut = int(len(data) * 0.75)
        return (data[0:index_cut], data[index_cut:])

    # Run PCA analysis on train_data.
    # Return both the train_data and test_data transformed.
    # Note: The test_data is not used for performing SVD.
    def run_pca(self, train_data, test_data, nc):
        n_features = len(train_data[0])-1
        pca = sklearn.decomposition.PCA(n_components=nc)
        pca.fit(train_data[:, 0:n_features])

        xform_data = pca.transform(train_data[:, 0:n_features])
        train_data = numpy.array([numpy.append(xform_data[i], train_data[i, n_features]) for i in range(0, len(xform_data))])

        xform_data = pca.transform(test_data[:, 0:n_features])
        test_data = numpy.array([numpy.append(xform_data[i], test_data[i, n_features]) for i in range(0, len(xform_data))])

        return (train_data, test_data)

    #================================================================
    # Configuration for running multiple classifiers.
    #================================================================

    # Return the list of classifiers to try.
    def get_classifiers(self):
        return [
            {
                "name": "QuadraticDiscriminant",
                "create": lambda: sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis()
            },
            {
                "name": "MLP(3)",
                "create": lambda: sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(19,19,19))
            },
            {
                "name": "MLP(4)",
                "create": lambda: sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(19,19,19,19))
            },
            {
                "name": "MLP(5)",
                "create": lambda: sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(19,19,19,19,19))
            },
            {
                "name": "DecisionTree(2)",
                "create": lambda: sklearn.tree.DecisionTreeClassifier(max_depth=2)
            },
            {
                "name": "DecisionTree(3)",
                "create": lambda: sklearn.tree.DecisionTreeClassifier(max_depth=3)
            },
            {
                "name": "DecisionTree(4)",
                "create": lambda: sklearn.tree.DecisionTreeClassifier(max_depth=4)
            },
            {
                "name": "DecisionTree(5)",
                "create": lambda: sklearn.tree.DecisionTreeClassifier(max_depth=5)
            },
            {
                "name": "DecisionTree(6)",
                "create": lambda: sklearn.tree.DecisionTreeClassifier(max_depth=6)
            },
            {
                "name": "AdaBoost",
                "create": lambda: sklearn.ensemble.AdaBoostClassifier()
            },
            {
                "name": "RandomForest",
                "create": lambda: sklearn.ensemble.RandomForestClassifier()
            },
            {
                "name": "GradientBoosting",
                "create": lambda: sklearn.ensemble.GradientBoostingClassifier()
            }
        ]
    
    #================================================================
    # Output and reporting for algorithm info
    #================================================================
    
    # Load data and train a decision tree without normalization to show
    # what a tree looks like.
    def train_and_report_decision_tree(self):
    	
    	# Load data
        print("Retrieving data from database.")
        sql = "SELECT * FROM V_SINGLE_DRIVER_FATALS"
        data, columns = self.read_data_sql(sql)

		# Pre process data: only unknown values are cleaned.
		# No normalization or PCA is applied so that decision tree
		# graphs can be reasoned about and discussed.
        print("Pre-processing data.")
        data = self.reset_unknown_values(data, columns)
        
        # Get the data in the shape needed for training.
        train_data, test_data = self.split_data(data)
        last_feature_idx = len(test_data[0]) - 1
        class_idx = len(test_data[0]) - 1
        observations = train_data[:, 0:last_feature_idx]
        class_values = train_data[:, class_idx]
        
        # Train a decision tree with different mas depths and output
        # the dot file.
        #
        # A dot file is a graph format which can be rendered using
        # the GraphViz tool.
        for n in [3, 4, 5]:
        	# Train a decision tree.
        	clf = sklearn.tree.DecisionTreeClassifier(max_depth=n)
        	clf.fit(observations, class_values)
        	
        	# Generate the dot file graph.
        	dot_file_name = "{}/decision_tree_{}.dot".format(self.output_dir, n)
        	print("Writing file: {}".format(dot_file_name))
        	sklearn.tree.export_graphviz(
        		clf,
        		out_file=dot_file_name,
        		feature_names=columns[0:last_feature_idx],
        		proportion=True,
        		rotate=True,
        		rounded=True,
        		filled=True,
        		class_names=['!DOA', 'DOA'])
        
    
    #================================================================
    # Running classification tests.
    #================================================================

    # Run the analysis.
    def run_all(self, times):
        print("Run classification analysis.")

        print("  Retrieving data from database.")

        sql = "SELECT * FROM V_SINGLE_DRIVER_FATALS"
        data, columns = self.read_data_sql(sql)

        print("  Pre-processing data.")

        data = self.reset_unknown_values(data, columns)
        data = self.normalize_values(data)

        # Run multiple tests
        for data_id in range(0, times):
            train_data, test_data = self.split_data(data)
            pca_train_data, pca_test_data = self.run_pca(train_data, test_data, 14)

            for classifier_spec in self.get_classifiers():
                self.run_test(classifier_spec, train_data, test_data, data_id, False)
                self.run_test(classifier_spec, pca_train_data, pca_test_data, data_id, True)

    # Run through 1 test run.
    def run_test(self, classifier_spec, train_data, test_data, data_id, using_pca):
        print("-----------------------------------------------------")
        print("  Start run: {}".format(datetime.datetime.now()))
        
        # Record information about this training and testing run.
        run = {}
        run["title"] = classifier_spec["name"]
        run["pca"] = using_pca
        run["data_id"] = data_id

        # Create the classifier.
        print("  Creating classifier: {}".format(run["title"]))
        clf = classifier_spec["create"]()

        # Train the classifier
        print("  Training classifier.")
        last_feature_idx = len(test_data[0]) - 1
        class_idx = len(test_data[0]) - 1
        start_time = datetime.datetime.now()
        clf.fit(train_data[:, 0:last_feature_idx], train_data[:, class_idx])
        run["training_time"] = (datetime.datetime.now() - start_time).total_seconds()
        
        # Do predictions
        print("  Running test predictions.")
        start_time = datetime.datetime.now()
        predicted = clf.predict(test_data[:, 0:last_feature_idx])
        run["prediction_time"] = (datetime.datetime.now() - start_time).total_seconds()
        actual = test_data[:, class_idx]

        # calculate stats
        tp = sum((predicted == 1) & (actual == 1))
        tn = sum((predicted == 0) & (actual == 0))
        fp = sum((predicted == 1) & (actual == 0))
        fn = sum((predicted == 0) & (actual == 1))

        run["accuracy"]   = 100.0 * (tp + tn) / len(test_data)
        run["precision"]  = 100.0 * tp / (tp + fp)
        run["recall"]     = 100.0 * tp / (fn + tp)
        run["neg_recall"] = 100.0 * tn / (tn + fp)
        run["fscore"]     = 2 / (1/run["recall"] + 1/run["precision"])
        
        print("  Run done: {}".format(datetime.datetime.now()))

        self.on_run_complete(run)

    # Run when a test run as completed.
    def on_run_complete(self, run):
        self.runs.append(run)
        for k in sorted(run.keys()):
            print("  {}: {}".format(k, run[k]))

    #================================================================
    # Computating and reporting for test run statistics.
    #================================================================

    # Write details about test runs.
    def write_test_run_info(self):
        self.write_test_run_detail()
        self.write_test_run_average_info()
        self.write_test_run_pca_diffs()

    def write_test_run_detail(self):
        file_name = "{}/test_run_details.csv".format(self.output_dir)
        self.write_csv(file_name, self.runs, [
            "title",
            "pca",
            "data_id",
            "fscore",
            "accuracy",
            "precision",
            "recall",
            "neg_recall",
            "training_time",
            "prediction_time"
        ])

    def write_test_run_average_info(self):
        averages = self.compute_run_averages()
        file_name = "{}/test_run_averages.csv".format(self.output_dir)
        self.write_csv(file_name, averages, [
            "title",
            "pca",
            "fscore",
            "accuracy",
            "precision",
            "recall",
            "neg_recall",
            "training_time",
            "prediction_time"
        ])
        
    def write_test_run_pca_diffs(self):
        diff = self.compute_pca_diffs()
        file_name = "{}/test_run_pca_diff.csv".format(self.output_dir)
        self.write_csv(file_name, diff, [
            "title",
            "fscore",
            "accuracy",
            "precision",
            "recall",
            "neg_recall"
        ])
        
    def compute_pca_diffs(self):
        averages = self.compute_run_averages()
        n_fields = [
            "fscore",
            "accuracy",
            "precision",
            "recall",
            "neg_recall",
            "training_time",
            "prediction_time"
        ]
        
        # Group by title and pca
        pca = {}
        npca = {}
        for avg in averages:
            if avg["pca"]:
                pca[avg["title"]] = avg
            else:
                npca[avg["title"]] = avg
        
        diffs = []
        for title in pca.keys():
            pca_avg = pca[title]
            npca_avg = npca[title]
            diff = { "title": title }
            for f in n_fields:
                diff[f] = npca_avg[f] - pca_avg[f]
            diffs.append(diff)
            
        return diffs
        
    def compute_run_averages(self):
        # Group the runs by pca and title.
        run_groups = {}
        for run in self.runs:
            k = (run["pca"], run["title"])
            if k not in run_groups:
                run_groups[k] = []
            run_groups[k].append(run)
        
        # Compute averages
        n_fields = [
            "training_time",
            "prediction_time",
            "accuracy",
            "precision",
            "recall",
            "neg_recall",
            "fscore"
        ]
        averages = []
        for k in run_groups.keys():
            runs = run_groups[k]
            entry = { "title": k[1], "pca": k[0] }
            for f in n_fields:
                entry[f] = sum(run[f] for run in runs) / len(runs)
            averages.append(entry)
        
        return averages

    def write_csv(self, file_name, objects, field_order=None):
        print("Writing info to: {}".format(file_name))
        if field_order == None:
            field_order = list(objects[0].keys())
        with open(file_name, "w") as fh:
            fh.write(",".join(field_order))
            fh.write("\n")
            for o in objects:
                fh.write(",".join([str(o[c]) for c in field_order]))
                fh.write("\n")
    
#================================================================
# Main: runnign the class as a script.
#================================================================
    
if __name__ == "__main__":
    classification = SDFClassification("data", "report/sdf_classification")
    classification.train_and_report_decision_tree()
    #classification.run_all(100)
    #classification.write_test_run_info()

