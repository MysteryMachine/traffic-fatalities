from mmachine.fars.file_sets import *
from mmachine.fars.schema_info import *
import os

# Read through FARS data.
class FarsSchemaInfo:

    def __init__(self, data_dir = "data"):
        self.data_dir = data_dir

    def run(self):
        rfs = RawFiles(self.data_dir)
        csv_file = os.path.join(rfs.get_dir(), 'schema_info.csv')
        si = SchemaInfo(self.data_dir)
        print("Writing schema info to: {}".format(csv_file))
        si.write_csv(si.get(), csv_file)

# The module is being run as a script
if __name__ == "__main__":
    FarsSchemaInfo().run()
