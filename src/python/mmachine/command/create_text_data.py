from mmachine.sqldb import *
import sys

class CreateTextData:

    def __init__(self, data_dir="data"):
        self.data_dir = data_dir

    def run(self, sql_name):
        db = SqlDb(self.data_dir)
        db.dump_text_data(sql_name)

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("ERROR Usage: run_create_text_data <SQL FILE NAME>")
        sys.exit(1)

    CreateTextData().run(sys.argv[1].strip())
