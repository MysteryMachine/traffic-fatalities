from mmachine.fars.sql import *

class FarsLoadDb:

    def run(self):
        sql_loader = SqlLoader("data")
        print("Creating schema for FARS data.")
        sql_loader.create_schema()
        sql_loader.load_data()

if __name__ == "__main__":
    FarsLoadDb().run()
