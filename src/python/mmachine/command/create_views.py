from mmachine.sqldb import *

class CreateViews:

    def __init__(self, data_dir="data"):
        self.data_dir = data_dir

    def run(self):
        db = SqlDb(self.data_dir)
        db.run_script("observation_views")

if __name__ == "__main__":
    CreateViews().run()
