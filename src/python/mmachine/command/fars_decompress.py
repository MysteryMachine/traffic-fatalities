from mmachine.fars.file_sets import *

# Extracts FARS download zip files into the raw data directory.
class FarsDecompress:

    def __init__(self, data_dir = "data"):
        self.data_dir = data_dir

    def run(self):
        dfs = DownloadFiles(self.data_dir)
        rfs = RawFiles(self.data_dir)
        for df in dfs.get_files():
            rfs.extract(df["year"], df["file"])

# The module is being run as a script
if __name__ == "__main__":
    FarsDecompress().run()
