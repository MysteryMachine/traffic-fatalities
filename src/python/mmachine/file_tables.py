import os
import csv

# Creates a table reader based on the file type.
class TableReader:

    # Open a table reader.
    @staticmethod
    def open(filename):
        lc = filename.lower()
        if lc.endswith(".csv"):
            return CSVTableReader(filename)
        elif lc.endswith(".dbf"):
            return DBFTableReader(filename)

    @staticmethod
    def open_with_type_detection(filename, max_rows = -1):
        # Detect the types from the file and wrap the table.
        tr = TableReader.open(filename)
        type_info = TableTypeInfo()
        type_info.detect(tr, max_rows)

        # Create a converter function with a reference back
        # to the type info.
        converter = lambda row: type_info.convert_row(row)
        setattr(converter, 'type_info', type_info)

        return ConvertingTableReader(tr, converter)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

class TableReaderIterator:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        pass

    def __iter__(self):
        return self

class ConvertingTableReader(TableReader):

    def __init__(self, table_reader, converter):
        self.header = table_reader.header
        self.converter = converter
        self.table_reader = table_reader

    def rows(self):
        return ConvertingTableReaderIterator(self.converter, self.table_reader.rows())

class ConvertingTableReaderIterator(TableReaderIterator):

    def __init__(self, converter, iterator):
        self.converter = converter
        self.iterator = iterator

    def close(self):
        self.iterator.close()

    def __next__(self):
        row = next(self.iterator)
        return self.converter(row)

# A table reader for CSV files
class CSVTableReader(TableReader):

    def __init__(self, filename):
        self.filename = filename
        # Get a copy of the header a head of time.
        with self.rows() as iter:
            self.header = iter.header

    def rows(self):
        return CSVTableIterator(self.filename)

class CSVTableIterator(TableReaderIterator):

    def __init__(self, filename):
        self.filename = filename
        self.io_handle = open(self.filename, 'r')
        self.csv_reader = csv.reader(self.io_handle)
        self.header = [s.strip().upper() for s in next(self.csv_reader)]

    def close(self):
        self.io_handle.close()

    def __next__(self):
        row = next(self.csv_reader)
        if row:
            rec = {}
            for i in range(0, len(self.header)):
                rec[self.header[i]] = row[i]
            return rec
        else:
            raise StopIteration

# A table reader for dbf files.
class DBFTableReader(TableReader):

    def __init__(self, filename):
        import dbfread
        self.filename = filename
        self.table = dbfread.DBF(self.filename)
        self.header = [s.strip().upper() for s in self.table.field_names]

    def rows(self):
        return DBFTableIterator(iter(self.table))

class DBFTableIterator(TableReaderIterator):

    def __init__(self, table_iterator):
        self.table_iterator = table_iterator

    def __next__(self):
        row = next(self.table_iterator)
        return { k.strip().upper():v for k,v in row.items() }

# Determine types for columns.

# basic type conversions for file table data.
class TypeConverters:

    @staticmethod
    def get_defaults():
        return [
            { 'converter': TypeConverters.int, 'type': int },
            { 'converter': TypeConverters.float, 'type': float },
            { 'converter': TypeConverters.str, 'type': str }
        ]

    @staticmethod
    def int(s):
        if s == None:
            return None
        elif isinstance(s, int):
            return s
        elif isinstance(s, str):
            s = s.strip()
            if s == "":
                return None
            else:
                return int(s)
        else:
            raise ValueError(s)

    @staticmethod
    def float(s):
        if s == None:
            return None
        elif isinstance(s, float):
            return s
        elif isinstance(s, int):
            return float(s)
        elif isinstance(s, str):
            s = s.strip()
            if s == "":
                return None
            else:
                return float(s)
        else:
            raise ValueError(s)

    @staticmethod
    def str(s):
        if s == None:
            return None
        elif isinstance(s, str):
            s = s.strip()
            if s == "":
                return None
            else:
                return s
        else:
            return str(s)

# Builds up possible type conversions for file table data.
class TableTypeInfo:

    def __init__(self, type_converters = TypeConverters.get_defaults()):
        self._type_info = {}
        self._type_converters = type_converters

    def get_types(self):
        types = {}
        for k,i in self._type_info.items():
            types[k] = self._type_converters[i]['type']
        return types

    def detect(self, table_reader, max_rows = -1):
        with table_reader.rows() as rows:
            row_no = 0
            for row in rows:
                for col in table_reader.header:
                    self._update_type_info(col, row[col])
                row_no += 1
                if max_rows >= 0 and row_no >= max_rows:
                    return

    def convert_row(self, row):
        nrow = {}
        for k,v in row.items():
            try:
                nrow[k] = self._type_converters[self._type_info[k]]['converter'](v)
            except Exception as e:
                raise ValueError("Could convert value {} for key {} - {}".format(v, k, e))
        return nrow

    def _update_type_info(self, col, value):
        vi = self._determine_index(value)
        if not col in self._type_info:
            self._type_info[col] = vi
        elif vi > self._type_info[col]:
            self._type_info[col] = vi

    def _determine_index(self, value):
        for i in range(0, len(self._type_converters)):
            converter = self._type_converters[i]
            try:
                converter['converter'](value)
                return i
            except Exception as e:
                pass
        raise Exception('Value not convertible: {}'.format(value))
