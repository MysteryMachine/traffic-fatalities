import os
import shutil

# Accessing and maniuplating the local database for mmachine.
class SqlDb:

    def __init__(self, data_dir):
        self.data_dir = data_dir

    def get_connection(self):
        # make the import lazy
        import sqlite3
        db_dir = os.path.join(self.data_dir, "db")
        if not os.path.isdir(db_dir):
            os.makedirs(db_dir)
        db_file = os.path.join(db_dir, "mmachine.db")
        print("db_file: {}".format(db_file))
        return sqlite3.connect(db_file)

    def run_script(self, sql_name):
        sql = self._get_sql(sql_name)
        with self.get_connection() as conn:
            print("Running: {}".format(sql_name))
            conn.executescript(sql)

    def dump_text_data(self, sql_name):
        sql = self._get_sql(sql_name)
        data_file = os.path.join(self.data_dir, sql_name + ".txt")
        data_meta_file = os.path.join(self.data_dir, sql_name + "_meta.txt")
        with self.get_connection() as conn:
            cur = conn.cursor()
            cur.execute(sql)
            with open(data_meta_file, "w") as fh:
                for col in cur.description:
                    fh.write(col[0] + "\n")
            with open(data_file, "w") as fh:
                print("Writing: {}".format(data_file))
                for row in cur:
                    for i in range(0, len(row)):
                        if i > 0:
                            fh.write(' ')
                        fh.write(self._text_cell(row[i]))
                    fh.write("\n")

    def _text_cell(self, value):
        if value == None:
            return "N/A"
        else:
            return str(value).replace(" ", "_")

    def _get_sql(self, sql_name):
        sql_file = os.path.join("src", "sql", sql_name + ".sql")
        with open(sql_file, "r") as fh:
            return fh.read()
