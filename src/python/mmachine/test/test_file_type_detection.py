from mmachine.file_tables import *

# Test to make sure that both a csv and dbf file are readable and convertible.

def run_file_test(filename):
    print("Examining file: {}".format(filename))
    tr = TableReader.open_with_type_detection(filename, 5000)
    print(tr.converter.type_info.get_types())
    print(tr.header)
    n = 0
    for row in tr.rows():
        print(row)
        n += 1
        if n >= 5:
            break

def run():
    run_file_test("data/FARS/raw/2016/accident.csv")
    run_file_test("data/FARS/raw/2010/accident.dbf")

if __name__ == "__main__":
    run()
