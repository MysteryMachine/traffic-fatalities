%------------------------------------------------------------------------------
% Script: hw2-p1.m
% Author: Bryan Castillo
%
% Description:
% 
% Examines the iris dataset and outputs the following:
%   1. iris_stats.csv - A csv with the mean and standard deviations
#      for each feature and species.
%   2. Graphs of the normal distribution for each feature showing each species
%      as a separate line. The image names are of the format:
%       feature_normal_distribution_<Feature>.png
%   3. A graph showing overlapping normalized frequencies with normal
%       normal distributions. bar_with_normal_dist.png
%
%------------------------------------------------------------------------------

% Tell octave this is a script file: not a function file.
1;

%------------------------------------------------------------------------------
% Computes the mean and standard deviation for all species and features.
%------------------------------------------------------------------------------
function species_stats = compute_iris_stats(iris)
    % Computes the mean and standard deviation for each feature of each species.
    species_stats = struct();
    for i = 1:length(iris.species)
        species_data = getfield(iris.data, iris.species{i});
        stats.mu  = mean(species_data);
        stats.st  = std(species_data);
        species_stats = setfield(species_stats, iris.species{i}, stats);
    endfor
endfunction

%------------------------------------------------------------------------------
% Prints out the iris statistics to a csv file.
% This makes it easy to input the statistics into a report.
%------------------------------------------------------------------------------
function print_iris_stats_csv(iris)
    % Print the means and standard deviations to the screen.
    fh = fopen("iris_stats.csv", "w");
    
    % Write top header of csv.
    for j = 1:length(iris.species)
        fprintf(fh, ",%s,", iris.species{j});
    endfor
    fprintf(fh, "\n");
    
    % Write secondary header of csv.
    for j = 1:length(iris.species),
        fprintf(fh, ",Mean,Std Dev.");
    endfor
    fprintf(fh, "\n");
    
    % Write rows
    for i = 1:length(iris.columns)
        fprintf(fh, "%s", iris.columns{i});
        for j = 1:length(iris.species);
            stats = getfield(iris.stats, iris.species{j});
            fprintf(fh, ",%f,%f", stats.mu(i), stats.st(i));
        endfor
        fprintf(fh, "\n");
    endfor
    fclose(fh);
endfunction


%------------------------------------------------------------------------------
% Creates the normal distribution graphs by feature.
%------------------------------------------------------------------------------
function create_feature_normal_dist_plots(iris)
    % Create plots
    for i = 1:length(iris.columns);
        clf reset;
        grid on;
        hold on;
        set(gca, "fontsize", 25);
        title(sprintf("%s Normal Distributions", iris.columns{i}));
        xlabel(sprintf("%s (cm)", iris.columns{i}));
        for j = 1:length(iris.species);
            species_name = iris.species{j};
            stats = getfield(iris.stats, species_name);
            mu = stats.mu(i);
            st = stats.st(i);
            x = linspace(mu - st*3, mu + st*3, 200);
            h = plot(x, normpdf(x, mu, st));
            set (h(1), "linewidth", 4);
        endfor
        lgnd = legend(iris.species);
        % https://www.mathworks.com/matlabcentral/answers/97850-how-do-i-make-the-background-of-a-plot-legend-transparent
        set(lgnd, 'color', 'none');
        legend("boxoff");
        print(sprintf("feature_normal_distribution_%s.png", iris.columns{   i}));
    endfor
endfunction

%------------------------------------------------------------------------------
% Creates normal distribution plots with each feature and species
% overlaid with a bar chart. The bar chard is the histogram where the
% area of the bar chart is normalized to 1. This lets you compare the
% bars with a guassian function.
%------------------------------------------------------------------------------
function create_bar_with_normal_dist_plot(iris)
    % Create plots
    clf reset;
    sp_idx = 1;
    for i = 1:length(iris.columns);
        for j = 1:length(iris.species);
            subplot(4,3,sp_idx);
            sp_idx = sp_idx + 1;
            hold on;
            
            species_name = iris.species{j};
            species_data = getfield(iris.data, species_name);
            
            d = species_data(:,i);
            min_d = min(d);
            max_d = max(d);
            step = (max_d - min_d) / 5;
            
            edges = min_d:step:max_d;
            counts = histc(d, edges);
            
            % Dividing by the sum and the width of the bar
            % makes sure that the area within the bars is 1.
            % this means it should match the area of a pdf and it makes
            % it easier to see if the actual data matches with a normal
            % distribution.
            bar(edges, counts ./ sum(counts) ./ step);
            
            stats = getfield(iris.stats, species_name);
            mu = stats.mu(i);
            st = stats.st(i);
            x = linspace(mu - st*3, mu + st*3, 100);
            plot(x, normpdf(x, mu, st));
            
            title(sprintf("%s: %s", species_name, iris.columns{i}));
            xlabel(sprintf("%s (cm)", iris.columns{i}));
        endfor
    endfor
    print("bar_with_normal_dist.png");
endfunction

%------------------------------------------------------------------------------
% Runs the main octave program.
%------------------------------------------------------------------------------
function main
    iris = struct();
    iris.data = load("iris.mat");
    iris.species = fieldnames(iris.data);
    iris.columns = {
        "Sepal Length",
        "Sepal Width",
        "Petal Length",
        "Petal Width"
    };
    iris.stats = compute_iris_stats(iris);
    print_iris_stats_csv(iris);
    create_feature_normal_dist_plots(iris);
    create_bar_with_normal_dist_plot(iris);
endfunction

main();
